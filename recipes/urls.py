from django.urls import path
from recipes.views import show_recipe
from recipes.views import recipe_list

urlpatterns = [
    path("recipes/", recipe_list),
    path("recipes/<int:id>/", show_recipe),
]
